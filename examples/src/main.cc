
#include "../include/c2d.h"
#include "../include/time.h"
#include <iostream>
#include "c2d.h"
#include "draw.h"
#include "sprite.h"
#include "input.h"

/*
Function to upde the solid shape points
*/
void updateBox(float pos_x, float pos_y , float* p) {
	const float box_size = 50.0f;
	p[0] = pos_x;
	p[1] = pos_y;

	p[2] = pos_x;
	p[3] = pos_y + box_size;

	p[4] = pos_x + box_size;
	p[5] = pos_y + box_size;

	p[6] = pos_x + box_size;
	p[7] = pos_y;

	p[8] = pos_x;
	p[9] = pos_y;

}

int main(int argc, char** argv) {

	float points[] = {
		0.0f,1.0f,
		-1.0f,-1.0f,
		0.0f,0.0f,
		1.0f,-1.0f,
		0.0f,1.0f
	};

	C2D::Geometry2D mesh_ = C2D::Create2DGeometry(5, points);
	C2D::Transform tr1 = {};
	tr1.SetPosition(100.0f, 100.0f);
	tr1.SetScale(40.0f, 40.0f);
	//Initialize library
	if (!C2D::Init(960, 704, "C2D Example",ALMath::vec3(0.0f))) {
		return 1;
	}

	double currentT = GetSecondsTime();
	double accum = 0.0;
	double dt = 0.01;
	double t = 0.0;

	 //Create a new sprite
	C2D::Sprite sprite1 = C2D::LoadSpriteFromFile("final/map_03_960x704_layout.bmp");

	//Set lines color
	C2D::SetColorStroke(255.0f, 0.0f, 0.0f);
	
	//Points buffer, there are 5 points beacuse the last point must be the first again.
	float p[10] = {
		0.0f,0.0f,
		0.0f,0.0f,
		0.0f,0.0f,
		0.0f,0.0f,
		0.0f,0.0f
	};

	bool solid = true;



	while (!C2D::ShouldClose()) {
		double new_time = GetSecondsTime();
		double frame_time = new_time - currentT;
		currentT = new_time;
		accum += frame_time;

		while (accum >= dt) {
			//Update points and get input.
			ALMath::vec2 xy = C2D::Input::GetMouseXY();
			updateBox(xy.x_, xy.y_, p);
			if (C2D::Input::GetKeyPressed('A') || C2D::Input::GetKeyPressed('1') || 
								C2D::Input::GetButtonMousePressed(C2D::Input::kMouseButtonRight)){
				solid = !solid;
			}
			accum -= dt;
			t += dt;

		}
		//Call draw commands
		C2D::BeginDraw();
		C2D::DrawSprite(sprite1, 0, 0);
		C2D::DrawLine(30.0, 50.0f, 100.0, 100.0f);
		C2D::DrawLine(100.0f,50.0f,30.0, 100.0f);
		C2D::DrawSolidShape(mesh_,false,tr1);
		C2D::EndDraw();
		//Call render frame
		C2D::RenderFrame();
	}
	C2D::Destroy();
	return 0;
}
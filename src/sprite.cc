#include "sprite.h"
#include "graphics/opengl_wrap.h"
#include "graphics/render_manager.h"
#include "graphics/draw_list.h"
#include "graphics/commands.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <assert.h>
#include "graphics/type_defs.h"


C2D::Sprite C2D::LoadSpriteFromFile(const char* file, bool flip_vertically) {



	int w, h, c;
	stbi_set_flip_vertically_on_load(flip_vertically);
	unsigned char* image = stbi_load(file, &w, &h, &c, 0);
	
	SpriteData *s_data = nullptr;
	if (image != nullptr) {
		s_data = new SpriteData(w, h, c,-1);
		s_data->data = image;

		Command create_sprite = {};
		create_sprite.cmds_.type = CommandType::CREATE_SPRITE_COMMAND;
		create_sprite.cmds_.create_sprite_cmd.sprite = s_data;

		RenderManager::Get()->draw_list_->AddCommand(create_sprite);
		return s_data;
	}else {
		std::cout << "Cannot load image from file" << std::endl;
		return nullptr;
	}
	
}

int C2D::SpriteHeight(Sprite& sp) {
	assert((SpriteData*)sp != nullptr, "Trying to access a null sprite");
 	return ((SpriteData*)sp)->height;
}

int C2D::SpriteWidth(Sprite& sp) {
	assert((SpriteData*)sp != nullptr, "Trying to access a null sprite");
	return ((SpriteData*)sp)->width;
}

int C2D::SpriteChannels(Sprite& sp) {
	assert((SpriteData*)sp != nullptr, "Trying to access a null sprite");
	return ((SpriteData*)sp)->channels;
}


void C2D::GetSpritePixel(Sprite & sprite, int x, int y,unsigned char* pix){
	assert((SpriteData*)sprite != nullptr, "Trying to render a null sprite");
	SpriteData* sdata = (SpriteData*)sprite;

	if (x <= sdata->width && y <= sdata->height) {
		int index = y * sdata->width + x;
		unsigned char*pixel = sdata->data + index * sdata->channels;
		pix[0] = pixel[0] ;
		pix[1] = pixel[1] ;
		pix[2] = pixel[2] ;
		pix[3] = sdata->channels >= 4 ? pixel[3] : 0xff;
	}
	else {
		pix[0] = pix[1] = pix[2] = pix[3] = 0xff;
	}

	

}

void C2D::DestroySprite(Sprite & sprite){
	assert((SpriteData*)sprite != nullptr, "Trying to render a null sprite");
	SpriteData* sdata = (SpriteData*)sprite;
	Graphics::DeleteTexture(sdata->gl_id);
	stbi_image_free(sdata->data);
	delete sdata;
}

unsigned int C2D::GetInternalID(Sprite& sprite) {
	SpriteData* sdata = (SpriteData*)sprite;
	return sdata->gl_id;
}

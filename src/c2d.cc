#include "../include/c2d.h"
#include "../deps/glfw/deps/glad/glad.h"
#include "../deps/glfw/include/GLFW/glfw3.h"
#include "../include/types.h"
#include "../include/input.h"
#include "internal_input.h"
#include "graphics/opengl_wrap.h"
#include "graphics/render_manager.h"

#include "graphics/commands.h"
#include "graphics/type_defs.h"
#include <map>
#include <iostream>

#include <iostream>

using namespace C2D;
enum InternalFlags {
	None = 0,
	WindowFullScreen = 1,
	WindowWindowed = 2,
	WindowMinimized = 4,
	WindowFocused = 8


};

struct  {
	///Window data
	GLFWwindow* window;
	int16 window_w;
	int16 window_h;
	ResizeWindowCallback resize_w_callback;
	char flags = 0;

}Data;


////////////// GLFW CALLBACKS //////////////

static void ErrorCallback(int code, const char* error) {
	std::cout << "GLFW ERROR (" << code << "): " << error << std::endl;
}

static void ResizeCallback(GLFWwindow*wdw, int w, int h) {
	Graphics::SetOrtho(ALMath::Ortho(0.0f, w, h,0.0f, -1.0f, 1.0f));
	glViewport(0, 0, w, h);
	if (Data.resize_w_callback != NULL) {
		Data.resize_w_callback(w, h);
	}
	Data.window_h = h;
	Data.window_w = w;
	if (glfwGetWindowAttrib(wdw, GLFW_MAXIMIZED)) {
		
	}
}
static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
	
	if (key < 0 || key > 1024)return;

	if (action == GLFW_PRESS)
		C2D::Input::KeyDown(key);
	if (action == GLFW_RELEASE)
		C2D::Input::KeyUp(key);


}

static void CursorPositionCallback(GLFWwindow* window, double xpos, double ypos){
	PrivateInput::g_mouse_x = xpos;
	PrivateInput::g_mouse_y = ypos;
}

void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods){
	
	if (action == GLFW_PRESS) {
		PrivateInput::g_mouse_buttons[button] = true;
	}else if(action == GLFW_RELEASE) {
		PrivateInput::g_mouse_buttons[button] = false;
	}	
}

////////////// INTERNAL FUNCTIONS //////////////


bool InitWindow(int16 width, int16 height, WindowInitOptions opts , const char* name, ALMath::vec3 bg_color) {
	if (!glfwInit()) {
		return false;
	}

	glfwSetErrorCallback(ErrorCallback);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE,( (opts & WindowInitOptions::WInitOptionsNotResizable) != 0) ? false: true);
	

	if ( (opts & WindowInitOptions::WInitOptionsFullScreen) != 0) {
		GLFWmonitor *monitor = glfwGetPrimaryMonitor();
		Data.window = glfwCreateWindow(width, height, name, monitor , NULL);
		const GLFWvidmode  *vmode = glfwGetVideoMode(monitor);
		Data.window_w = vmode->width;
		Data.window_h = vmode->height;
		Data.flags |= InternalFlags::WindowFullScreen;
	}else  if((opts & WindowInitOptions::WInitOptionsFitScreen) != 0) {
		GLFWmonitor *monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode  *vmode = glfwGetVideoMode(monitor);
		Data.window_w = vmode->width;
		Data.window_h = vmode->height;
		Data.window = glfwCreateWindow(Data.window_w, Data.window_h, name, NULL, NULL);

	}else {
		Data.window_w = width;
		Data.window_h = height;
		Data.window = glfwCreateWindow(width, height, name, NULL, NULL);
	}
	
	if (Data.window == nullptr) {
		glfwTerminate();
		return false;
	}

	glfwMakeContextCurrent(Data.window);
	glfwSwapInterval(1);
	glfwSetWindowSizeCallback(Data.window, ResizeCallback);
	glfwSetKeyCallback(Data.window, KeyCallback);
	glfwSetCursorPosCallback(Data.window, CursorPositionCallback);
	glfwSetMouseButtonCallback(Data.window, MouseButtonCallback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cout << "ERROR ( GLAD cannot be initialized" << std::endl;
		glfwTerminate();
		return false;
	}

	Data.window_w = width;
	Data.window_h = height;
	glViewport(0, 0, width, height);
	glClearColor(bg_color.r_, bg_color.g_, bg_color.b_, 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return true;
}



////////////// NAMESPACE FUNCTIONS //////////////



bool C2D::Init(int16 width, int16 height, const char* name, ALMath::vec3 bg_color, WindowInitOptions opts, ResizeWindowCallback  c) {
	
	if (!InitWindow(width, height,opts, name,bg_color) ||
		!Graphics::InitGraphics(ALMath::Ortho(0.0f, width, height, 0.0f, -1.0f, 1.0f)) ) return false;
	RenderManager::Get()->Init();
	Data.resize_w_callback = c;


	C2D::Font arial = C2D::LoadFont("fonts/arial.ttf");
	C2D::UseFont(arial);
	return true;
}

bool C2D::ShouldClose() {
	bool result =  glfwWindowShouldClose(Data.window);

	if ( result &&  glfwGetWindowMonitor(Data.window)) {
		glfwSetWindowMonitor(Data.window,NULL,0,0,0,0,0);
	}else{
		return result;
	}
}

void C2D::PollEvents(){
	glfwPollEvents();
}

void C2D::RenderFrame() {
	glfwSwapBuffers(Data.window);
}

void C2D::Destroy() {
	Graphics::DestroyGraphics();
	glfwTerminate();
}

int C2D::GetWindowWidth() {
	return Data.window_w;
}

int C2D::GetWindowHeight() {
	return Data.window_h;
}



Font C2D::LoadFont(const char* path) {
	FT_Face face;
	if (FT_New_Face(RenderManager::Get()->free_type_l, path, 0, &face)) {
		std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
		return nullptr;
	}

	FontData *fdata = new FontData();
	FT_Set_Pixel_Sizes(face, 0, 48);
	Graphics::GlPixelStore1();

	// Disable byte-alignment restriction
	for (unsigned char i = 0; i < 128; i++) {
		if (FT_Load_Char(face, i, FT_LOAD_RENDER)) {
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
			continue;
		}

		unsigned int id = Graphics::CreateGlyphTexture(face->glyph->bitmap.buffer, face->glyph->bitmap.width, face->glyph->bitmap.rows);


		Glyph character = {
			id,
			ALMath::vec2(face->glyph->bitmap.width,face->glyph->bitmap.rows),
			ALMath::vec2(face->glyph->bitmap_left,face->glyph->bitmap_top),
			(unsigned int)face->glyph->advance.x,face->glyph->bitmap.buffer
		};

		fdata->glyphs.insert(std::pair<char, Glyph>(i, character));

	}

	FT_Done_Face(face);


	Command load_font = {};
	load_font.cmds_.create_font_cmd.type = CommandType::CREATE_FONT_COMMAND;
	load_font.cmds_.create_font_cmd.face = face;
	load_font.cmds_.create_font_cmd.font = fdata;

	//RenderManager::Get()->draw_list_->AddCommand(load_font);

	return fdata;


}
int C2D::NumGlyphs(const Font& font) {
	return ((FontData*)&font)->glyphs.size();
}

void C2D::UseFont(const Font& font) {
	assert(&font && "Trying to use a null font");
	C2D::Font f = font;
	RenderManager::Get()->using_font = (FontData*)f;
}
void C2D::DeleteFont(const Font& font) {
	assert(&font && "Trying to delete a null font");
	delete &font;
}

void * C2D::WindowPtr(){
	return Data.window;
}

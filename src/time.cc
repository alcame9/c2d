#include "../include/time.h"
#include "../deps/glfw/include/GLFW/glfw3.h"

double GetSecondsTime(){
  return glfwGetTime();
}

double GetMiliTime(){
  return glfwGetTime() * 0.001;
}



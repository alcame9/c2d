#ifndef __PRIVATE_INPUT_H__
#define __PRIVATE_INPUT_H__
namespace C2D {

	namespace PrivateInput {
		extern bool g_keys[1024];
		extern bool g_mouse_buttons[3];
		extern bool close_window_button;
		extern float g_mouse_y;
		extern float g_mouse_x;
	}

}
#endif // !__PRIVATE_INPUT_H__

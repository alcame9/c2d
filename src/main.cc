
/*
@author Alejandro Canela 2018.
@email a.canelame@gmail.com
*/


#include <iostream>
#include "c2d.h"
#include "draw.h"
#include "input.h"
#include "sprite.h"

void PressDCallBack(void* d ) {
	C2D::Transform* t = (C2D::Transform*)d;

	float step = 360.0f / 1.5f;
	float rot = t->Rotation();

	const float rotation_speed = 300.0f;
	float rotation =  step * 0.16;
	float total_rot = rot + rotation;

	if (total_rot > 360.0f) total_rot = 0.0f;
	if (total_rot < -360.0f) total_rot = 0.0f;

	t->SetRotation(total_rot);
	
}

int main(int argc, char** argv) {

	C2D::WindowInitOptions wdw_init_opt = C2D::WInitOptionsFitScreen;
	if (!C2D::Init(800, 600, "alGUINO",wdw_init_opt)) {
		system("pause");
	}

	C2D::Sprite sprite1 = C2D::LoadSpriteFromFile("enemyShip.png");
	C2D::SetColorStroke(255.0f, 0.0f, 0.0f);
	
	/*LOCAL COORDS*/
	float p[] = {
		0.0f,1.0f,
		-1.0f,-1.0f,
		0.0f,0.0f,
		1.0f,-1.0f,
		0.0f,1.0f
	};

	float p1[] = {
		40.0f,40.0f,
		40.0f, 80.0f,
		80.0f, 80.0f,
		80.0f, 40.0f,
		40.0f, 40.0f
	};
	C2D::Geometry2D ss = C2D::Create2DGeometry(5,p);
	C2D::Geometry2D ss2 = C2D::Create2DGeometry(5, p1);
	C2D::Transform tr1 = {};
	tr1.SetPosition(ALMath::vec2(110.0f, 110.0f));
	tr1.SetScale(ALMath::vec2(50.0f, 50.0f));
	tr1.SetRotation(180.0f);

	C2D::Input::AddKeyPressedCallback('A', Press DCallBack, &tr1);

	while (!C2D::ShouldClose()) {
		C2D::PollEvents();

		C2D::BeginDraw();
		C2D::DrawSprite(sprite1, 0, 0);
		unsigned char pixel[4];
		C2D::SetColorFillShape(255.0f, 0.0f, 0.0f);
		C2D::DrawSolidShape(ss,false,tr1);
		C2D::SetColorFillShape(255.0f, 255.0f, 0.0f);


		C2D::DrawLine(300.0, 300.0, 380.0, 300.0f);
		C2D::SetColorStroke(255.0f, 255.0f, 0.0f);
		C2D::DrawLine(100.0, 100.0, 180.0, 100.0f);

		C2D::DrawText(450.0f, 50.0f, "Text Example 434",1.0f, ALMath::vec3(0.0f, 0.0f, 0.0f));
		C2D::DrawText(450.0f,450.0f, "Text Example 2", 0.5f, ALMath::vec3(255.0f, 0.0f, 0.0f));

		C2D::EndDraw();

		C2D::RenderFrame();
	}
	return 0;
}
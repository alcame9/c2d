#include "../include/draw.h"
#include "graphics/opengl_wrap.h"
#include "graphics/render_manager.h"
#include "graphics/commands.h"
#include <assert.h>
#include <ft2build.h>


struct{
	ALMath::vec3 color_stroke = ALMath::vec3(1.0f);
	ALMath::vec3 color_fill = ALMath::vec3(1.0f);
}DrawData;

void C2D::BeginDraw() {
	Command clear_buff = {};
	clear_buff.cmds_.type = CommandType::PREPARE_RENDER_COMMAND;
	clear_buff.cmds_.prepare_render_cmd.clear_type_ = Graphics::COLOR_BUFFER_BIT_;
	RM->draw_list_->AddCommand(clear_buff);
}

void C2D::EndDraw() {
	RM->FinalRender();
}

Geometry2D C2D::Create2DGeometry(int number_points, float * points){
	
	assert(number_points != 0 && "Trying to initialize a  geometry with zero points");

	Geom2D *g =  new Geom2D();
	g->Init(points, number_points);
	Graphics::Create2DGeometry(*g);

	return g;
}


int C2D::Geometry2DTotalPoints(Geometry2D g) {
	Geom2D* s = (Geom2D*)g;
	assert(g && "null shape");
	return s->number_points;
}
float* C2D::Geometry2DPoints(Geometry2D g) {
	Geom2D* s = (Geom2D*)g;
	assert(s && "null shape");
	return s->local_points;
}

static void  IDrawSolidShape(C2D::Geom2D* g, ALMath::mat4 model, bool filled, ALMath::vec3 c) {

	Command render_line = {};
	render_line.cmds_.type = CommandType::RENDER_SOLID_SHAPE_COMMAND;
	render_line.cmds_.render_solid_shape_cmd.geo = g;
	render_line.cmds_.render_solid_shape_cmd.filled = filled;
	render_line.cmds_.render_solid_shape_cmd.color = c;
	render_line.cmds_.render_solid_shape_cmd.model = model;
	RM->draw_list_->AddCommand(render_line);
}

//void C2D::DrawSolidShape(Geometry2D shape, bool filled){
//	Geom2D* g = (Geom2D*)shape;
//	assert( g && "null shape");
//	IDrawSolidShape(g->ogl_vao,g->number_points,filled, DrawData.color_fill);
//
//}

void C2D::DrawSolidShape(Geometry2D shape, bool filled, Transform& transform){

	Geom2D* g = (Geom2D*)shape;
	assert(g && "null shape");
	IDrawSolidShape(g,transform.Model(),filled, DrawData.color_fill);
}

void C2D::DrawLine(int x1, int y1, int x2, int y2){
	Command render_line = {};
	render_line.cmds_.type = CommandType::RENDER_LINE_COMMAND;
	render_line.cmds_.render_line_cmd.x1 = x1;
	render_line.cmds_.render_line_cmd.y1 = y1;
	render_line.cmds_.render_line_cmd.x2 = x2;
	render_line.cmds_.render_line_cmd.y2 = y2;
	render_line.cmds_.render_line_cmd.color = DrawData.color_stroke;
	RM->draw_list_->AddCommand(render_line);
}


void C2D::DrawSprite(Sprite& sprite, int x, int y, float angle) {
	
	ALMath::mat4 transform;
	ALMath::vec2 size = ALMath::vec2(C2D::SpriteWidth(sprite), C2D::SpriteHeight(sprite));
	ALMath::mat4 translate;
	ALMath::TranslateMat4(translate, ALMath::vec3(x, y, 0.0f));

	ALMath::mat4 translate_center;
	ALMath::TranslateMat4(translate_center, ALMath::vec3(size.x_ * 0.5f, size.y_ * 0.5f, 0.0f));
	transform = translate * translate_center;

	ALMath::mat4 rotation;
	ALMath::RotateMat4(rotation, ALMath::vec3(0.0f, 0.0f, 1.0f), angle);
	transform = transform * rotation;

	ALMath::mat4 translate_back;
	ALMath::TranslateMat4(translate_back, ALMath::vec3(size.x_ * -0.5f, size.y_ * -0.5f, 0.0f));
	transform = transform * translate_back;

	ALMath::mat4 scale;
	ALMath::ScaleMat4(scale, ALMath::vec3(size, 1.0f));
	transform = transform * scale;


	DrawSprite(sprite, transform);
}

void C2D::DrawSprite(Sprite& sprite, ALMath::mat4 transform_mat) {
	assert(&sprite != nullptr, "Trying to render a null sprite");
	Command render_sprite = {};
	render_sprite.cmds_.type = CommandType::RENDER_SPRITE_COMMAND;
	render_sprite.cmds_.render_sprite_cmd.id = C2D::GetInternalID(sprite);
	render_sprite.cmds_.render_sprite_cmd.transform = transform_mat;
	RM->draw_list_->AddCommand(render_sprite);
}

void C2D::SetColorStroke(float r, float g, float b){
	DrawData.color_stroke = ALMath::vec3(r, g , b ) / 255.0f;
}

void C2D::SetColorFillShape(float r, float g, float b){
	DrawData.color_fill = ALMath::vec3(r, g, b) / 255.0f;
}
void C2D::RenderText(int x, int y, const char* text, float scale, ALMath::vec3 color) {
	RenderText(ALMath::vec2(x, y), text, scale, color);
}

void C2D::RenderText(ALMath::vec2 pos, const char* text, float scale, ALMath::vec3 color) {
	Command render_text = {};
	render_text.cmds_.type = CommandType::RENDER_TEXT_COMMAND;
	memcpy(render_text.cmds_.render_text_cmd.text,text,strlen(text)* sizeof(char));
	render_text.cmds_.render_text_cmd.fdata = RenderManager::Get()->using_font;
	render_text.cmds_.render_text_cmd.color = color;
	render_text.cmds_.render_text_cmd.x = pos.x_;
	render_text.cmds_.render_text_cmd.y = pos.y_;
	render_text.cmds_.render_text_cmd.scale = scale;

	RM->draw_list_->AddCommand(render_text);
}
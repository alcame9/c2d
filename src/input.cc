#include "../include/input.h"
#include "internal_input.h"
#include "../include/c2d.h"

#include "../deps/glfw/include/GLFW/glfw3.h"
#include <map>
#include <vector>

#include <string>
#include <utility>
using namespace C2D;
using namespace ALMath;

bool PrivateInput::g_keys[1024];
bool PrivateInput::g_mouse_buttons[3];
float PrivateInput::g_mouse_y;
float PrivateInput::g_mouse_x;
bool PrivateInput::close_window_button = false;



struct {
	std::map<int, std::vector<std::pair<Input::InputCallback,void*>>> key_downs;
	std::map<int, std::vector<std::pair<Input::InputCallback, void*>>> key_ups;

	std::map<int, std::vector<std::pair<Input::InputCallback, void*>>> key_pressed;
	std::map<int, std::vector<std::pair<Input::InputCallback, void*>>> key_released;
}InputData;



const bool Input::GetButtonMousePressed(Input::MouseButtons button) {
	switch (button) {
	case Input::kMouseButtonsLeft:
		return PrivateInput::g_mouse_buttons[0];
		break;
	case Input::kMouseButtonRight:
		return PrivateInput::g_mouse_buttons[1];
		break;
	case Input::kMouseButtonMiddle:
		return PrivateInput::g_mouse_buttons[2];
		break;
	default:
		return false;
		break;
	}
	return false;
}

const float Input::GetMouseX() {
	return PrivateInput::g_mouse_x;
}
const float Input::GetMouseY() {
	return PrivateInput::g_mouse_y;
}
const vec2 Input::GetMouseXY() {
	return vec2(PrivateInput::g_mouse_x, PrivateInput::g_mouse_y);
}
const bool Input::GetKeyPressed(char key_i) {

	int asci = (int)key_i;


	return true;
	/*std::string character = "";
	std::transform(character.begin(), character.end(), character.begin(), ::toupper);
	const char* key = character.c_str();
	bool is_pressed = false;
	if (strcmp("A", key) == 0 && PrivateInput::g_keys[GLFW_KEY_A] == true) {
		is_pressed = true;
	}
	else if (strcmp("B", key) == 0 && PrivateInput::g_keys[GLFW_KEY_B] == true) {
		is_pressed = true;
	}
	else if (strcmp("C", key) == 0 && PrivateInput::g_keys[GLFW_KEY_C] == true) {
		is_pressed = true;
	}
	else if (strcmp("D", key) == 0 && PrivateInput::g_keys[GLFW_KEY_D] == true) {
		is_pressed = true;
	}
	else if (strcmp("E", key) == 0 && PrivateInput::g_keys[GLFW_KEY_E] == true) {
		is_pressed = true;
	}
	else if (strcmp("F", key) == 0 && PrivateInput::g_keys[GLFW_KEY_F] == true) {
		is_pressed = true;
	}
	else if (strcmp("G", key) == 0 && PrivateInput::g_keys[GLFW_KEY_G] == true) {
		is_pressed = true;
	}
	else if (strcmp("H", key) == 0 && PrivateInput::g_keys[GLFW_KEY_H] == true) {
		is_pressed = true;
	}
	else if (strcmp("I", key) == 0 && PrivateInput::g_keys[GLFW_KEY_I] == true) {
		is_pressed = true;
	}
	else if (strcmp("J", key) == 0 && PrivateInput::g_keys[GLFW_KEY_J] == true) {
		is_pressed = true;
	}
	else if (strcmp("K", key) == 0 && PrivateInput::g_keys[GLFW_KEY_K] == true) {
		is_pressed = true;
	}
	else if (strcmp("L", key) == 0 && PrivateInput::g_keys[GLFW_KEY_L] == true) {
		is_pressed = true;
	}
	else if (strcmp("M", key) == 0 && PrivateInput::g_keys[GLFW_KEY_M] == true) {
		is_pressed = true;
	}
	else if (strcmp("N", key) == 0 && PrivateInput::g_keys[GLFW_KEY_N] == true) {
		is_pressed = true;
	}
	else if (strcmp("O", key) == 0 && PrivateInput::g_keys[GLFW_KEY_O] == true) {
		is_pressed = true;
	}
	else if (strcmp("P", key) == 0 && PrivateInput::g_keys[GLFW_KEY_P] == true) {
		is_pressed = true;
	}
	else if (strcmp("Q", key) == 0 && PrivateInput::g_keys[GLFW_KEY_Q] == true) {
		is_pressed = true;
	}
	else if (strcmp("R", key) == 0 && PrivateInput::g_keys[GLFW_KEY_R] == true) {
		is_pressed = true;
	}
	else if (strcmp("S", key) == 0 && PrivateInput::g_keys[GLFW_KEY_S] == true) {
		is_pressed = true;
	}
	else if (strcmp("T", key) == 0 && PrivateInput::g_keys[GLFW_KEY_T] == true) {
		is_pressed = true;
	}
	else if (strcmp("U", key) == 0 && PrivateInput::g_keys[GLFW_KEY_U] == true) {
		is_pressed = true;
	}
	else if (strcmp("V", key) == 0 && PrivateInput::g_keys[GLFW_KEY_V] == true) {
		is_pressed = true;
	}
	else if (strcmp("W", key) == 0 && PrivateInput::g_keys[GLFW_KEY_W] == true) {
		is_pressed = true;
	}
	else if (strcmp("X", key) == 0 && PrivateInput::g_keys[GLFW_KEY_X] == true) {
		is_pressed = true;
	}
	else if (strcmp("Y", key) == 0 && PrivateInput::g_keys[GLFW_KEY_Y] == true) {
		is_pressed = true;
	}
	else if (strcmp("Z", key) == 0 && PrivateInput::g_keys[GLFW_KEY_Z] == true) {
		is_pressed = true;
	}
	else if (strcmp("0", key) == 0 && PrivateInput::g_keys[GLFW_KEY_0] == true) {
		is_pressed = true;
	}
	else if (strcmp("1", key) == 0 && PrivateInput::g_keys[GLFW_KEY_1] == true) {
		is_pressed = true;
	}
	else if (strcmp("2", key) == 0 && PrivateInput::g_keys[GLFW_KEY_2] == true) {
		is_pressed = true;
	}
	else if (strcmp("3", key) == 0 && PrivateInput::g_keys[GLFW_KEY_3] == true) {
		is_pressed = true;
	}
	else if (strcmp("4", key) == 0 && PrivateInput::g_keys[GLFW_KEY_4] == true) {
		is_pressed = true;
	}
	else if (strcmp("5", key) == 0 && PrivateInput::g_keys[GLFW_KEY_5] == true) {
		is_pressed = true;
	}
	else if (strcmp("6", key) == 0 && PrivateInput::g_keys[GLFW_KEY_6] == true) {
		is_pressed = true;
	}
	else if (strcmp("7", key) == 0 && PrivateInput::g_keys[GLFW_KEY_7] == true) {
		is_pressed = true;
	}
	else if (strcmp("8", key) == 0 && PrivateInput::g_keys[GLFW_KEY_8] == true) {
		is_pressed = true;
	}
	else if (strcmp("9", key) == 0 && PrivateInput::g_keys[GLFW_KEY_9] == true) {
		is_pressed = true;
	}
	return is_pressed;*/
}
const bool Input::IsSpecialKeyPressed(SpecialKeys special_key) {
	switch (special_key) {

	case Input::kSpecialKeysEsc:
		return PrivateInput::g_keys[GLFW_KEY_ESCAPE];
		break;
	case Input::kSpecialKeysSpace:
		return PrivateInput::g_keys[GLFW_KEY_SPACE];
		break;
	case Input::kSpecialKeysEnter:
		return PrivateInput::g_keys[GLFW_KEY_ENTER];
		break;
	case Input::kSpecialKeysRightControl:
		return PrivateInput::g_keys[GLFW_KEY_RIGHT_CONTROL];
		break;
	case Input::kSpecialKeysLeftControl:
		return PrivateInput::g_keys[GLFW_KEY_LEFT_CONTROL];
		break;
	case Input::kSpecialKeysRightShift:
		return PrivateInput::g_keys[GLFW_KEY_RIGHT_SHIFT];
	case Input::kSpecialKeysLeftShift:
		return PrivateInput::g_keys[GLFW_KEY_LEFT_SHIFT];
		break;
	case Input::kSpecialKeyLeftArrow:
		return PrivateInput::g_keys[GLFW_KEY_LEFT];
	case Input::kSpecialKeyRightArrow:
		return PrivateInput::g_keys[GLFW_KEY_RIGHT];
	case Input::kSpecialKeyUpArrow:
		return PrivateInput::g_keys[GLFW_KEY_UP];
	case Input::kSpecialKeyDownArrow:
		return PrivateInput::g_keys[GLFW_KEY_DOWN];

		break;
	default:
		return false;
		break;
	}
	return false;
}




void C2D::Input::AddKeyPressedCallback(int key, InputCallback cb, void *data) {
	InputData.key_pressed[key].push_back(std::pair<InputCallback, void*>(cb, data));
}


void C2D::Input::AddKeyReleaseCallback(int key, InputCallback cb, void *data) {
	InputData.key_released[key].push_back(std::pair<InputCallback, void*>(cb, data));
}




void C2D::Input::RemoveKeyCallback(int key){
}

void C2D::Input::KeyDown(int key){
	for (std::pair<InputCallback,void*> &c : InputData.key_pressed[key]) {
		c.first(c.second);
	}
}


void C2D::Input::KeyUp(int key) {
	for (std::pair<InputCallback, void*> &c : InputData.key_released[key]) {
		c.first(c.second);
	}
}

bool C2D::Input::KeyPressed(char key) {
	int r = glfwGetKey((GLFWwindow*)C2D::WindowPtr(), key);
	
	if (r == GLFW_PRESS)return true;
	return false;
	
}

bool C2D::Input::KeyReleased(char key) {
	int r = glfwGetKey((GLFWwindow*)C2D::WindowPtr(), key);
	if (r == GLFW_RELEASE)return true;
	return false;
}


void SetButtonMousePressed(Input::MouseButtons button, bool pressed) {
	switch (button) {
	case Input::kMouseButtonsLeft:
		PrivateInput::g_mouse_buttons[0] = pressed;
		break;
	case Input::kMouseButtonRight:
		PrivateInput::g_mouse_buttons[1] = pressed;
		break;
	case Input::kMouseButtonMiddle:
		PrivateInput::g_mouse_buttons[2] = pressed;
		break;
	default:
		break;
	}
}


#include "draw_list.h"
#include "commands.h"


static void ExecuteCommand(Command& cmd);

DrawList::DrawList() {}

DrawList::~DrawList() {}

void DrawList::AddCommand(Command cmd) {
	commands_.push_back(cmd);
}

void DrawList::Execute() {
	int total_cmds = commands_.size();
	for (int i = 0; i < total_cmds; i++) {
		ExecuteCommand(commands_[i]);
	}
	Clear();
}

void DrawList::Clear() {
	commands_.clear();
}

void ExecuteCommand(Command& cmd) {
	switch (cmd.cmds_.type) {
	case CommandType::PREPARE_RENDER_COMMAND:
		Graphics::ClearBuffer(cmd.cmds_.prepare_render_cmd.clear_type_);
		break;
	case CommandType::USE_PROGRAM:
		Graphics::UseProgram(cmd.cmds_.use_program_cmd.program);
		break;
	case CommandType::RENDER_SPRITE_COMMAND:
		Graphics::RenderSprite(cmd.cmds_.render_sprite_cmd.id, cmd.cmds_.render_sprite_cmd.transform);
		break;
	case CommandType::RENDER_LINE_COMMAND:
		Graphics::RenderLine(cmd.cmds_.render_line_cmd.x1,cmd.cmds_.render_line_cmd.y1,
							 cmd.cmds_.render_line_cmd.x2,cmd.cmds_.render_line_cmd.y2,
							 cmd.cmds_.render_line_cmd.color);
		break;
	case CommandType::RENDER_SOLID_SHAPE_COMMAND:
		Graphics::RenderSolidShape(cmd.cmds_.render_solid_shape_cmd.geo,
			cmd.cmds_.render_solid_shape_cmd.model,
			cmd.cmds_.render_solid_shape_cmd.color,
			cmd.cmds_.render_solid_shape_cmd.filled);
		break;
	case CommandType::RENDER_TEXT_COMMAND:
		Graphics::RenderText(cmd.cmds_.render_text_cmd.x, cmd.cmds_.render_text_cmd.y,
							cmd.cmds_.render_text_cmd.text,cmd.cmds_.render_text_cmd.scale,
							cmd.cmds_.render_text_cmd.color,cmd.cmds_.render_text_cmd.fdata);
		break;
	case CommandType::FINISH_RENDER_COMMAND:
		break;
	case CommandType::CREATE_SPRITE_COMMAND:
	{

		SpriteData& spr = *static_cast<SpriteData*>(cmd.cmds_.create_sprite_cmd.sprite);
		spr.gl_id = Graphics::CreateTexture(spr.data,spr.width,spr.height,spr.channels);
		if (spr.gl_id< 0) {
			delete &spr;
			std::cout << "Cannot load image from file" << std::endl;
		}
	}

		break;

	case CommandType::CREATE_FONT_COMMAND:
	{

		FontData& spr = *static_cast<FontData*>(cmd.cmds_.create_font_cmd.font);
		

		//FT_Set_Pixel_Sizes(f, 0, 48);
		//Graphics::GlPixelStore1();

		std::map<unsigned char, Glyph>::iterator it;
		for (it = spr.glyphs.begin(); it != spr.glyphs.end(); it++) {

			Glyph &gl = (it->second);

		}


		if (spr.glyphs.size() == 0) {
			delete &spr;
		}

	}

	break;
	default:
		break;
	}
}

#ifndef TYPE_DEFS_
#define TYPE_DEFS_
#include <map>
#include "../../include/types.h"
#include <ft2build.h>
#include FT_FREETYPE_H
namespace C2D {
	struct SpriteData;
	struct Glyph;
	struct FontData;

	struct SpriteData {
		int width;//
		int height;
		int channels;
		unsigned int gl_id;
		unsigned char* data;
		bool loaded;
		SpriteData(int w, int h, int c, int glid) {
			width = w;
			height = h;
			channels = c;
			gl_id = glid;
			loaded = false;
		}

	};


	struct Glyph {
		unsigned int ogl_id;
		ALMath::vec2 size;       // Size of glyph
		ALMath::vec2 bearing;    // Offset from baseline to left/top of glyph
		unsigned int advance; // Offset to advance to next glyph
		unsigned char* buff;
		Glyph() {}
		Glyph(unsigned int id, ALMath::vec2 s, ALMath::vec2 b, unsigned int a,unsigned char* bff) {
			ogl_id = id;
			size = s;
			bearing = b;
			advance = a;
			buff = bff;
		}
	};

	struct FontData {
		std::map<unsigned char, Glyph> glyphs;
	};

	struct Geom2D {
		Geom2D() {

		}
		void Init(float* p, int count) {
			number_points = count;
			total_size = sizeof(float) * number_points * 2;
			local_points = (float*)malloc(total_size);
			memcpy(local_points,p, total_size);
			
		}
		void Destroy(){
			delete local_points;
		}

		float* local_points;
		int number_points;
		int total_size;

		unsigned int ogl_vao;
		unsigned int ogl_buff;

	};

}




#endif
#ifndef _GRAPHICS_
#define _GRAPHICS_

#include "../../include/types.h"
#include "../../include/math_L.h"
#include "../../src/graphics/type_defs.h"
#include <string>
#include <iostream>
namespace Graphics {
	enum OGLTypes {
		OGL_UINT = 0x1405,
		OGL_UBYTE = 0x1401,
		OGL_BYTE = 0x1400,
		OGL_INT = 0x1404,
		OGL_FLOAT = 0x1406,
		OGL_DOUBLE = 0x140A,
		OGL_BOOL = 0x8B56

	};

	enum ShaderType {
		VERTEX_SHADER = 0x8B31,
		FRAGMENT_SHADER = 0x8B30,
		GEOMETRY_SHADER = 0x8DD9,

	};

	enum BufferType {
		ARRAY_BUFFER = 0x8892,
		ELEMENT_ARRAY_BUFFER = 0x8893,
		UNIFORM_BUFFER = 0x8A11,

	};

	enum  BufferUsage {
		STATIC_DRAW = 0x88E4,
		DYNAMIC_DRAW = 0x88E8,


	};

	enum DrawMode {
		DRAWMODE_POINTS = 0x0000,
		DRAWMODE_LINES = 0x0001,
		DRAWMODE_LINELOOP = 0x0002,
		DRAWMODE_LINE_STRIP = 0x0003,
		DRAWMODE_TRIANGLES = 0x0004,
		DRAWMODE_TRIANGLE_STRIP = 0x0005,
		DRAWMODE_TRIANGLE_FAN = 0x0006,

	};
	enum BufferBitType {
		COLOR_BUFFER_BIT_ = 0x00004000,
		DEPTH_BUFFER_BIT = 0x00000100,
		STENCIL_BUFFER_BIT = 0x00000400

	};
	
	void SetOrtho(ALMath::mat4 o);

	//////////////////////// CLEAR FUNCTIONS ////////////////////////

	void ClearBuffer(BufferBitType buff_type);
	void ClearColor(float color[4]);

	//////////////////////// SHADER FUNCTIONS ////////////////////////

	bool CompileShader(unsigned int shader_id);

	bool CreateProgram(string vertex_data, string fragment_data, unsigned int& program_id);

	void UseProgram(unsigned int program);


	//////////////////////// TEXTURE FUNCTIONS ////////////////////////

	unsigned int CreateTexture(unsigned char* data, int width, int height, int channels);
	unsigned int CreateGlyphTexture(unsigned char* data, int width, int height);
	void DeleteTexture(unsigned int id);

	void CreateSpriteQuad();


	//////////////// RENDER FUNCTIONS ///////////////

	void RenderSprite(int id_texture, ALMath::mat4 sprite_transform);

	void RenderLine(int x1, int y1, int x2, int y2,ALMath::vec3 color);

	void RenderSolidShape(C2D::Geom2D* g,ALMath::mat4 model, ALMath::vec3 color,bool filled);

	void RenderText(int x, int y, const char* text, float scale, ALMath::vec3 color, C2D::FontData *fdata);
	//////////////// INIT FUNCTIONS ///////////////

	bool InitOpenGLData();

	bool InitGLESData();

	bool InitGraphics(ALMath::mat4 o);

	void DestroyGraphics();
	//////////////// GETTER FUNCTIONS ///////////////
	int GetSpriteProgramId();

	void GlPixelStore1();

	//////////////// BUFFER FUNCTIONS ///////////////

	void Create2DGeometry(C2D::Geom2D& g);



}


#endif
#include "render_manager.h"
#include <iostream>
#include <ft2build.h>
RenderManager* RenderManager::ptr_ = nullptr;
RenderManager::RenderManager() {
	draw_list_ = new DrawList();
}

RenderManager::~RenderManager() {
	delete draw_list_;
}

void RenderManager::Init(){
	if (FT_Init_FreeType(&free_type_l))
		std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
}

void RenderManager::Update() {

}

void RenderManager::FinalRender() {
	draw_list_->Execute();
}

 RenderManager* RenderManager::Get() {
	if (ptr_ == nullptr) ptr_ = new RenderManager();
	return ptr_;
}
#ifndef _DRAW_LIST_
#define _DRAW_LIST_
#include <vector>
struct Command;
class DrawList {
public:
	DrawList();
	~DrawList();
	void AddCommand(Command cmd);
	void Execute();
	void Clear();
private:
	std::vector<Command> commands_;
};
#endif // !_DRAW_LIST_

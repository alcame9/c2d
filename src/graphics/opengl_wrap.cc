#include "opengl_wrap.h"
#include "../../include/c2d.h"
#include "../../deps/glfw/deps/glad/glad.h"
using namespace C2D;
static struct {
	//Programs ids
	unsigned int sprite_program;
	unsigned int lines_program;
	unsigned int text_program;
	//uniform ids
	unsigned int sprite_program_mvp;
	unsigned int line_program_mvp;
	unsigned int line_color_u;
	unsigned int text_projection_u;
	unsigned int text_color_u;
	unsigned int text_sampler_u;
	//VAO ids
	unsigned int sprite_quad;
	unsigned int lines_buff_vao;
	unsigned int solid_shape_vao;
	unsigned int text_vao;
	//Buffers ids
	unsigned int lines_vbo;
	unsigned int solid_shape_vbo;
	unsigned int text_vbo;
	//Misc
	ALMath::mat4 ortho;

}GraphicalData;


void Graphics::SetOrtho(ALMath::mat4 o) {
	GraphicalData.ortho = o;
}

//////////////////////// CLEAR FUNCTIONS ////////////////////////

void Graphics::ClearBuffer(BufferBitType buff_type) {
	glClear(buff_type);
}

void Graphics::ClearColor(float color[4]) {
	glClearColor(color[0], color[1], color[2], color[3]);
}

//////////////////////// SHADER FUNCTIONS ////////////////////////

bool Graphics::CompileShader(unsigned int shader_id) {
	glCompileShader(shader_id);
	GLint is_compiled;
	GLchar info_log[512];
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &is_compiled);
	//Check compile status
	if (is_compiled == GL_FALSE) {
		glGetShaderInfoLog(shader_id, 512, NULL, info_log);
		GLint shader_type;
		glGetShaderiv(shader_id, GL_SHADER_TYPE, &shader_type);
		switch (shader_type) {
		case GL_VERTEX_SHADER:
		{
			std::string msg = "VERTEX SHADER COMPILED ERROR: \n";
			std::string info(info_log);
			info += "\n";
			msg += info;
			std::cout << msg.c_str() << std::endl;
			break;
		}
		case GL_FRAGMENT_SHADER:
		{
			std::string msg = "FRAGMENT SHADER COMPILED ERROR: \n";
			std::string info(info_log);
			info += "\n";
			msg += info;
			std::cout << msg.c_str() << std::endl;
			break;
		}
		case GL_GEOMETRY_SHADER:
		{
			std::string msg = "FRAGMENT SHADER COMPILED ERROR: \n";
			std::string info(info_log);
			info += "\n";
			msg += info;
			std::cout << msg.c_str() << std::endl;
			break;
		}
		default:
			break;
		}
		return false;
	}
	return true;
}

bool Graphics::CreateProgram(string vertex_data, string fragment_data, unsigned int& program_id) {


	GLuint shader_f;
	GLuint shader_v;

	shader_v = glCreateShader(GL_VERTEX_SHADER);
	shader_f = glCreateShader(GL_FRAGMENT_SHADER);
	GLint lenght = vertex_data.length();
	const GLchar * data = vertex_data.data();
	glShaderSource(shader_v, 1, &data, &lenght);
	if (!CompileShader(shader_v))return false;

	lenght = fragment_data.length();
	data = fragment_data.data();
	glShaderSource(shader_f, 1, &data, &lenght);
	if (!CompileShader(shader_f))return false;

	GLuint program = glCreateProgram();
	glAttachShader(program, shader_v);
	glAttachShader(program, shader_f);

	glLinkProgram(program);
	GLint linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE) {
		GLchar log[512];
		glGetProgramInfoLog(program, 512, NULL, log);
		std::cout << "Program not compiled. Error : " << log << std::endl;
		return false;
	}
	glDeleteShader(shader_v);
	glDeleteShader(shader_f);
	program_id = program;
	return true;
}

void Graphics::UseProgram(unsigned int program) {
	glUseProgram(program);
}



//////////////////////// TEXTURE FUNCTIONS ////////////////////////

unsigned int Graphics::CreateTexture(unsigned char* data, int width, int height, int channels) {
	GLuint text_id;
	glGenTextures(1, &text_id);
	glBindTexture(GL_TEXTURE_2D, text_id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	GLenum internal_format = GL_RGBA;
	switch (channels) {
	case 1:
		internal_format = GL_R32F;
		break;
	case 2:
		internal_format = GL_RG;
		break;
	case 3:
		internal_format = GL_RGB;
		break;
	case 4:
		internal_format = GL_RGBA;
		break;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, internal_format, GL_UNSIGNED_BYTE, data);
	return text_id;
}

unsigned int Graphics::CreateGlyphTexture(unsigned char * data, int width, int height){
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_RED,
		width,
		height,
		0,
		GL_RED,
		GL_UNSIGNED_BYTE,
		data
	);
	// Set texture options
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D,0);
	return texture;
}

void Graphics::DeleteTexture(unsigned int id) {
	glDeleteTextures(1, &id);
}


void Graphics::CreateSpriteQuad() {
	float vertices[] = {
		0.0f, 1.0f, /*POS-UVS*/  1.0f, 0.0f,
		1.0f, 0.0f, /*POS-UVS*/ 0.0f, 1.0f,
		0.0f, 0.0f, /*POS-UVS*/ 1.0f, 1.0f,

		0.0f, 1.0f, /*POS-UVS*/ 1.0f, 0.0f,
		1.0f, 1.0f,/*POS-UVS*/  0.0f, 0.0f,
		1.0f, 0.0f,/*POS-UVS*/  0.0f, 1.0f

	};


	GLuint vao;
	glGenVertexArrays(1, &vao);
	GLuint vbo;
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_STATIC_DRAW);


	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);

	GraphicalData.sprite_quad = vao;

	//Create lines buffer

	GLuint vao_lines;
	glGenVertexArrays(1, &vao_lines);
	GLuint vbo_lines;
	glGenBuffers(1, &vbo_lines);

	glBindVertexArray(vao_lines);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_lines);
	glBufferData(GL_ARRAY_BUFFER, 4 *  sizeof(float), NULL, GL_STATIC_DRAW);

	GLuint elemts_vbo;
	GLuint order[2] = { 0,1 };
	glGenBuffers(1, &elemts_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elemts_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2 * sizeof(unsigned int), &order[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
	glBindVertexArray(0);
	GraphicalData.lines_buff_vao = vao_lines;
	GraphicalData.lines_vbo = vbo_lines;
	//Create lines buffer

	GLuint vao_solid;
	glGenVertexArrays(1, &vao_solid);
	GLuint vbo_solid;
	glGenBuffers(1, &vbo_solid);

	glBindVertexArray(vao_solid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_solid);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	GraphicalData.solid_shape_vao = vao_solid;
	GraphicalData.solid_shape_vbo = vbo_solid;


}



//////////////// RENDER FUNCTIONS ///////////////

void Graphics::RenderSprite(int id_texture, ALMath::mat4 sprite_transform) {
	glUseProgram(GraphicalData.sprite_program);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, id_texture);
	ALMath::mat4 mvp = GraphicalData.ortho * sprite_transform;
	glUniformMatrix4fv(GraphicalData.sprite_program_mvp, 1, GL_FALSE, &mvp[0][0]);
	glBindVertexArray(GraphicalData.sprite_quad);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}

void Graphics::RenderLine(int x1, int y1, int x2, int y2, ALMath::vec3 color){
	float points[4] = { x1,y1,x2,y2};
	glBindBuffer(GL_ARRAY_BUFFER, GraphicalData.lines_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * sizeof(float), points);
	glBindBuffer(GL_ARRAY_BUFFER,0);


	glUseProgram(GraphicalData.lines_program);
	glUniformMatrix4fv(GraphicalData.line_program_mvp, 1, GL_FALSE, &GraphicalData.ortho[0][0]);
	glUniform3f(GraphicalData.line_color_u, color.x_, color.y_, color.z_);
	glBindVertexArray(GraphicalData.lines_buff_vao);
	glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, (void*)0);
	glBindVertexArray(0);
}

void Graphics::RenderSolidShape(C2D::Geom2D* g, ALMath::mat4 model,ALMath::vec3 color,bool filled){

	//glBindBuffer(GL_ARRAY_BUFFER, GraphicalData.solid_shape_vbo);
	//glBufferData(GL_ARRAY_BUFFER, 2 * number_points * sizeof(float), p,GL_DYNAMIC_DRAW);
	//glBindBuffer(GL_ARRAY_BUFFER, 0);

	glUseProgram(GraphicalData.lines_program);
	ALMath::mat4 mvp = GraphicalData.ortho * model;
	glUniformMatrix4fv(GraphicalData.line_program_mvp, 1, GL_FALSE, &mvp[0][0]);
	glUniform3f(GraphicalData.line_color_u, color.x_, color.y_, color.z_);
	glBindVertexArray(g->ogl_vao);
	(filled) ? glDrawArrays(GL_TRIANGLE_STRIP, 0,g->number_points) : glDrawArrays(GL_LINE_STRIP, 0, g->number_points);
	glBindVertexArray(0);
}

void Graphics::RenderText(int x, int y, const char * text, float scale, ALMath::vec3 color,C2D::FontData *fdata){

	glUseProgram(GraphicalData.text_program);

	glUniform3f(GraphicalData.text_color_u, color.r_, color.g_, color.b_);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(GraphicalData.text_vao);
	string txt(text);
	std::string::const_iterator c;
	for (c = txt.begin(); c != txt.end(); c++) {
		Glyph gl = fdata->glyphs[*c];
		GLfloat xpos = x + gl.bearing.x_ * scale;
		GLfloat ypos = y - (gl.size.y_ - gl.bearing.y_) * scale;

		GLfloat w = gl.size.x_ * scale;
		GLfloat h = gl.size.y_ * scale;
		// Update VBO for each character

		GLfloat vertices[6][4] = {
	{ xpos,     ypos + h,   0.0, 0.0 },
{ xpos,     ypos,       0.0, 1.0 },
{ xpos + w, ypos,       1.0, 1.0 },

{ xpos,     ypos + h,   0.0, 0.0 },
{ xpos + w, ypos,       1.0, 1.0 },
{ xpos + w, ypos + h,  1.0, 0.0 }
};
		//GLfloat vertices[6][4] = {
		//	{ xpos,   ypos  ,   0.0, 0.0 },
		//{ xpos,     ypos + h,       0.0, 1.0 },
		//{ xpos + w, ypos + h,       1.0, 1.0 },

		//{ xpos,     ypos ,   0.0, 0.0 },
		//{ xpos + w, ypos + h,       1.0, 1.0 },
		//{ xpos + w, ypos,   1.0, 0.0 }
		//};

		// Render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, gl.ogl_id);
		glBindBuffer(GL_ARRAY_BUFFER, GraphicalData.text_vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // Be sure to use glBufferSubData and not glBufferData
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (gl.advance >> 6) * scale;
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);

}





//////////////// INIT FUNCTIONS ///////////////
bool Graphics::InitOpenGLData() {
	const char sprite_vertex[] = {

		"#version 330\n"
		"layout(location = 0) in vec4 vertex;\n"
		"out vec2 uv_out;\n"
		"uniform mat4 mvp;\n"

		"void main() {\n"
		"uv_out = vertex.zw;\n"
		"gl_Position = mvp * vec4(vertex.xy,0.0,1.0);\n"
		"}\n"

	};

	const char sprite_fragment[] = {
		"#version 330\n"
		"in vec2 uv_out; \n"
		"out vec4 color; \n"
		"uniform sampler2D u_texture; \n"

		"void main() {\n"
		"color = texture(u_texture,uv_out) ; \n"
		"}\n"

		
	};
	
	if (!CreateProgram(sprite_vertex, sprite_fragment, GraphicalData.sprite_program)) {
		std::cout << "Graphic Error during sprite shader creation" << std::endl;
		return false;
	}



	//Lines shader


	const char lines_vertex[] = {

		"#version 330\n"
		"layout(location = 0) in vec2 vertex;\n"
		
		"uniform mat4 mvp;\n"

		"void main() {\n"
		
			"gl_Position = mvp * vec4(vertex,0.0,1.0);\n"
		"}\n"

	};

	const char lines_fragment[] = {
		"#version 330\n"

		"out vec4 outcolor; \n"
		"uniform vec3 color; \n"

		"void main() {\n"
			"outcolor = vec4(color,1.0f); \n"
		"}\n"


	};

	if (!CreateProgram(lines_vertex, lines_fragment, GraphicalData.lines_program)) {
		std::cout << "Graphic Error during lines shader creation" << std::endl;
		return false;
	}

	GraphicalData.sprite_program_mvp = glGetUniformLocation(GraphicalData.sprite_program, "mvp");
	GraphicalData.line_program_mvp = glGetUniformLocation(GraphicalData.lines_program, "mvp");
	GraphicalData.line_color_u = glGetUniformLocation(GraphicalData.lines_program, "color");


	const char text_vertex[] = {

		"#version 330\n"
		"layout(location = 0) in vec4 vertex;\n"
		"out vec2 uvs;\n"
		"uniform mat4 projection;\n"

		"void main() {\n"
		"gl_Position = projection * vec4(vertex.xy,0.0,1.0);\n"
		"uvs = vertex.zw;\n"
		"}\n"

	};

	const char text_fragment[] = {
		"#version 330\n"

		"out vec4 outcolor; \n"
		"uniform vec3 color; \n"
		"uniform sampler2D text;\n"
		"in vec2 uvs;\n"
		"void main() {\n"
		"vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, uvs).r);\n"
			"outcolor = vec4(color,1.0f) * sampled; \n"
		"}\n"


	};	

	if (!CreateProgram(text_vertex, text_fragment, GraphicalData.text_program)) {
		std::cout << "Graphic Error during text shader creation" << std::endl;
		return false;
	}

	GraphicalData.text_projection_u = glGetUniformLocation(GraphicalData.text_program, "projection");
	ALMath::mat4 ortho = ALMath::Ortho(0.0f, C2D::GetWindowWidth(), 0.0f, C2D::GetWindowHeight(), -1.0f, 1.0f);
	glUseProgram(GraphicalData.text_program);
	glUniformMatrix4fv(GraphicalData.text_projection_u, 1, false, &ortho[0][0]);
	GraphicalData.text_color_u = glGetUniformLocation(GraphicalData.text_program, "color");
	GraphicalData.text_sampler_u = glGetUniformLocation(GraphicalData.text_program, "text");
	glUseProgram(0);

	CreateSpriteQuad();
	

	{//Text buffer creation
		glGenVertexArrays(1, &GraphicalData.text_vao);
		glGenBuffers(1, &GraphicalData.text_vbo);
		glBindVertexArray(GraphicalData.text_vao);
		glBindBuffer(GL_ARRAY_BUFFER, GraphicalData.text_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	return true;

}

bool Graphics::InitGLESData() {
	const char vertex[] = {
		"attribute vec2 a_position; \n"
		"attribute vec2 a_uv; \n"

		"uniform mat4 u_modelViewProjectionMatrix; \n"
		"varying vec2 uv; \n"
		"void main(){\n"
		"uv = a_uv; \n"
		"gl_Position = u_modelViewProjectionMatrix * vec4(a_position,0.0f,1.0f); \n"
		"}\n"

	};

	const char fragment[] = {
		"uniform vec3 color; \n"
		"void main(){\n"
		"gl_FragColor = vec4(color,1.0f);\n"
		"}\n"
	};

	if (!CreateProgram(vertex, fragment, GraphicalData.sprite_program)) {
		std::cout << "Graphic Error during sprite shader creation" << std::endl;
		return false;
	}


	return true;
}

bool Graphics::InitGraphics(ALMath::mat4 o) {
	
	if (!InitOpenGLData()) {
		return false;
	}
	GraphicalData.ortho = o;
	return true;

}
void Graphics::DestroyGraphics() {
	glDeleteBuffers(1,&GraphicalData.solid_shape_vbo);
	glDeleteBuffers(1,&GraphicalData.lines_vbo);

	glDeleteVertexArrays(1, &GraphicalData.sprite_quad);
	glDeleteVertexArrays(1, &GraphicalData.lines_buff_vao);
	glDeleteVertexArrays(1, &GraphicalData.solid_shape_vao);

	glDeleteProgram(GraphicalData.lines_program);
	glDeleteProgram(GraphicalData.sprite_program);
}
//////////////// GETTER FUNCTIONS ///////////////
int Graphics::GetSpriteProgramId() {
	return GraphicalData.sprite_program;
}

void Graphics::GlPixelStore1(){
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
}


void Graphics::Create2DGeometry(C2D::Geom2D& g) {
	GLuint vao;
	GLuint data;

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &data);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, data);
	glBufferData(GL_ARRAY_BUFFER, g.total_size, g.local_points, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	g.ogl_vao = vao;
	g.ogl_buff = data;
	
}

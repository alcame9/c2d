#ifndef _RENDER_MANAGER_
#define _RENDER_MANAGER_
#include "type_defs.h"
#define RM RenderManager::Get() 

#include "draw_list.h"


class RenderManager {
public:
	DrawList* draw_list_;
	FT_Library free_type_l;
	C2D::FontData* using_font;



	~RenderManager();
	void Init();
	void Update();

	void FinalRender();

	static RenderManager* Get();
private:
	RenderManager();
	static RenderManager* ptr_;
};

#endif // !_RENDER_MANAGER_

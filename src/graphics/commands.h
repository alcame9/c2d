/*
@author Alejandro Canela 2018.
@email a.canelame@gmail.com
*/

#ifndef COMMAND_H_
#define COMMAND_H_
#include "opengl_wrap.h"
#include "type_defs.h"
#include "types.h"
using namespace C2D;
	enum CommandType {
		PREPARE_RENDER_COMMAND = 0,
		USE_PROGRAM,
		RENDER_SPRITE_COMMAND,
		RENDER_LINE_COMMAND,
		RENDER_SOLID_SHAPE_COMMAND,
		RENDER_TEXT_COMMAND,
		FINISH_RENDER_COMMAND,

		CREATE_SPRITE_COMMAND,
		CREATE_FONT_COMMAND
	};


	//////////////COMMANDS////////////////

	struct PrepareRenderCommand {
		CommandType type;
		Graphics::BufferBitType  clear_type_;

	};
	
	struct RenderSpriteCommand {
		CommandType type;
		unsigned int id;
		ALMath::mat4 transform;
	};

	struct RenderLineCommand {
		CommandType type;
		int x1, y1;
		int x2, y2;
		ALMath::vec3 color;
	};

	struct RenderSolidShapeCommand {
		CommandType type;
		C2D::Geom2D* geo;
		ALMath::mat4 model;
		ALMath::vec3 color;
		bool filled;

	};

	struct RenderTextCommand {
		CommandType type;
		ALMath::vec3 color; 
		float scale;
		int x, y;
		char text[1024];
		FontData* fdata;
		

	};

	struct UseProgramCommand {
		CommandType type;
		unsigned int program;
	};

	struct CreateFontCommand {
		CommandType type;
		FT_Face face;
		FontData *font;
	};

	struct CreateSpriteCommand {
		CommandType type;
		SpriteData* sprite;

	};

	struct FinishRenderCommand {
		CommandType type;
	};
	/*
	*
	*/

	struct Command {
		union Commands {
			CommandType type;
				PrepareRenderCommand prepare_render_cmd;
				RenderSpriteCommand render_sprite_cmd;
				RenderLineCommand render_line_cmd;
				RenderSolidShapeCommand render_solid_shape_cmd;
				RenderTextCommand render_text_cmd;
				UseProgramCommand use_program_cmd;
				FinishRenderCommand finish_render_cmd;

				CreateFontCommand create_font_cmd;
				CreateSpriteCommand create_sprite_cmd;
		};
		Commands cmds_;
	};

#endif // !COMMAND_H_

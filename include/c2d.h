/*Copyright 2018 . Alejandro Canela Mendez
 * @ author Alejandro Canela Mendez
 * @brief Main header of the library
*/
#ifndef _C2D_
#define _C2D_
#include "types.h"

namespace C2D {

	enum WindowInitOptions {
		wInitOptionsNone = 0,
		WInitOptionsFullScreen = 1, //Specifies if window should ocupied the full screen when created
		WInitOptionsNotResizable =  2, //Specifies if window can be resized, resizable by default.
		WInitOptionsVisible = 4, //Specifies if window is visible when created, set by default.
		WInitOptionsFocused = 8, //Specifies if window is focused when created, set by default
		WInitOptionsFitScreen = 16 //Specifies if window should ocupied all the screen.

	};


	typedef void* Font;



	typedef void(*ResizeWindowCallback)(int w, int h);

	/*
	* @brief   Call this function to initialize the library
	* @param width   Window width
	* @param height   Window height
	* @param window_title   Window title
	* @return true if everything is correct or false if somethig goes wrong
	during the initialization
	*/
	bool Init(int16 width, int16 height, const char* window_title,  ALMath::vec3 bg_color, WindowInitOptions opts = WindowInitOptions(NULL), ResizeWindowCallback = NULL);

	/*
	* @brief  This function calls all render commands. 
	* Must be called after all draw functions

	*/
	void RenderFrame();

	/*
	* @brief  This function returns if the window should close
	* @return True if the window is going to close
	*/
	bool ShouldClose();
	/*
	* @brief  
	* @return 
	*/
	void PollEvents();
	/*
	@brief   Call this function to destroy all graphic resources.
			Shoud be called at the end of the rogram
	*/
	void Destroy();
	/*
	@brief   Get Window Width
	*/
	int GetWindowWidth();

	/*
	@brief   Get window height

	*/
	int GetWindowHeight();

	bool IsMaximized();

	Font LoadFont(const char* path);

	int NumGlyphs(const Font& font);

	void UseFont(const Font& font);

	void DeleteFont(const Font& font);

	void* WindowPtr();
}

#endif // !C2D
/*
* Copyright 2018. Alejandro Canela.
* Author : Alejandro Canela Mendez - a.canelame@gmail.com
*
* Brief : Sprite manager header
*/
#ifndef _SPRITE_
#define _SPRITE_
#include "math_L.h"
namespace C2D {
	typedef void*  Sprite;

	/*
	* @brief  Load a sprite from file
	* @param path   Path to the resource
	* @return A pointer a with a loaded sprite or nullptr if can not be loaded.
	*/
	Sprite LoadSpriteFromFile(const char* path,bool flip_vertically);

	/*
	* @brief   Return the width of a sprite
	* @param sprite   Desired sprite 
	* @return The width in pixels
	*/
	int SpriteWidth(Sprite& sprite);

	/*
	* @brief   Return the height of a sprite
	* @param sprite   Desired sprite
	* @return The the in pixels
	*/
	int SpriteHeight(Sprite& sprite);

	/*
	* @brief   Return the number of components of a sprite
	* @param sprite   Desired sprite
	* @return The number of components  ( 3 : RGB or 4 : RGBA )
	*/
	int SpriteChannels(Sprite& sprite);


	/*
	* @brief   Get Sprite pixel color in texture coordinates
	* @param sprite Desired Sprite
	* @param x  X coordinate
	* @param y  Y coordinate
	* @param pixel Output variable to store the color
	*/
	void GetSpritePixel(Sprite& sprite, int x, int y, unsigned char* pixel);

	/*
	* @brief   Destroy the sprite resource and free the memory
	* @param sprite Desired sprite
	*/
	void DestroySprite(Sprite& sprite);

	/*
	* @brief   Destroy the sprite resource and free the memory
	* @param sprite Desired sprite
	*/
	unsigned int GetInternalID(Sprite& sprite);
};

#endif // !_SPRITE_

/*Copyright 2018 . Alejandro Canela Mendez
* @ author Alejandro Canela Mendez
* @brief Main header for input handling
*/


#ifndef __INPUT_H__
#define __INPUT_H__
#include <memory>
#include "types.h"
#include <algorithm>
#include <functional>

namespace C2D {
	namespace Input {
		enum SpecialKeys {
			kSpecialKeysEsc = 0,
			kSpecialKeysSpace,
			kSpecialKeysEnter,
			kSpecialKeysRightControl,
			kSpecialKeysLeftControl,
			kSpecialKeysRightShift,
			kSpecialKeysLeftShift,
			kSpecialKeyLeftArrow,
			kSpecialKeyRightArrow,
			kSpecialKeyUpArrow,
			kSpecialKeyDownArrow
		};
		enum MouseButtons {
			kMouseButtonsLeft = 0,
			kMouseButtonRight,
			kMouseButtonMiddle
		};


		/*
		* @brief   Gets the mouse buttons events
		* @param button   Number button
		* @return true if presed false if released
		*/
		const bool GetButtonMousePressed(MouseButtons button);

		/*
		* @brief   Gets X cursor position in pixels
		* @return return X axis cursor position
		*/
		const float GetMouseX();

		/*
		/*
		* @brief   Gets Y cursor position in pixels
		* @return return Y axis cursor position
		*/
		const float GetMouseY();

		/*
		/*
		/*
		* @brief   Gets XY cursor position in pixels
		* @return return XY axis cursor position
		*/
		const ALMath::vec2 GetMouseXY();

		/*
		* @brief   Returns if a key has been pressd or released
		* @param key   Key, (A,B,C a,b,c,2,3,4...)
		* @return true if is pressed false if released
		*/
		const bool GetKeyPressed(char key);

		/*
		* @brief   Returns if a special key has been pressd or released
		* @param key   Key, (Ctrl, Space, Shift)
		* @return true if is pressed false if released
		*/
		const bool IsSpecialKeyPressed(SpecialKeys special_key);


		typedef  std::function<void(void*)> InputCallback;


		void AddKeyPressedCallback(int key, InputCallback cb, void *data);
		void AddKeyReleaseCallback(int key, InputCallback cb, void *data);

		void RemoveKeyCallback(int key);
		void KeyDown(int key);
		void KeyUp(int key);

		bool KeyPressed(char key);
		bool KeyReleased(char key);
	};
}
#endif // !__INPUT_H__


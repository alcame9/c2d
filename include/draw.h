
/*Copyright 2018 . Alejandro Canela Mendez
* @ author Alejandro Canela Mendez
* @brief Main header of the library
*/

#ifndef _DRAW_
#define _DRAW_
#include "math_L.h"
#include "sprite.h"

namespace C2D {
	
	typedef void* Geometry2D ;



	class Transform {
	public:
		Transform() {
			position_ = ALMath::vec2(0.0f);
			scale_ = ALMath::vec2(1.0f);
			SetRotation(0.0f);
			
		}

		Transform(ALMath::vec2 p, ALMath::vec2 s, float r) {
			position_ =p;
			scale_ = s;
			SetRotation(r);

		}

		void SetPosition(ALMath::vec2 p) { position_ = p; CalculateModel(); }
		void SetPosition(float x, float y) { SetPosition(ALMath::vec2(x, y)); }
		void SetScale(ALMath::vec2 s) { scale_ = s; CalculateModel(); }
		void SetScale(float x, float y) { SetScale(ALMath::vec2(x, y)); }
		void SetRotation(float rot) { 
			rotation_ = rot; CalculateModel();
			float rad_rot = RADIANS(rotation_);
			float s = sin(rad_rot);
			float c = cos(rad_rot);
			forward_.x_ = -s;
			forward_.y_ = c;

		}

		ALMath::vec2 Position() { return position_; }
		ALMath::vec2 Scale() { return scale_; }
		ALMath::vec2 Forward() { return forward_; }
		float Rotation() { return rotation_; }
	
		ALMath::mat4 Model() { return model_; }

	private:
		ALMath::mat4 model_;
		ALMath::vec2 position_;
		ALMath::vec2 scale_;
		ALMath::vec2 forward_;
		float rotation_;

		void CalculateModel() {
			ALMath::mat4 trans = {};
			ALMath::mat4 rot = {};
			ALMath::mat4 scal = {};

			ALMath::TranslateMat4(trans, ALMath::vec3(position_,0.0f));
			ALMath::ScaleMat4(scal, ALMath::vec3(scale_,1.0f));
			ALMath::RotateMat4(rot, ALMath::vec3(0.0f, 0.0f, 1.0f),RADIANS(rotation_));
			model_ = trans * rot * scal;
		}
	};
	
	/*
	* @brief   Call this function before call any draw funtion
				to prepare the render pass

	*/
	void BeginDraw();

	/*
	* @brief   Call this function after all draw functions but
	before C2D::RenderFrame(); .
	*/
	void EndDraw();


	/*
	* @brief Creates a new 2D shape composed by points
	* @param number_points   Number of points in the shape
	* @param points   Array with the points
	*
	* float points [] {
	point1_x, point1_y,
	point2_x_, point2_y,
	point3_x_,point3_y,

	};

	void Create2DGeometry(3,points);

	*/
	Geometry2D Create2DGeometry(int number_points, float* points);
	
	/*
	* @brief Destroy 2 geometry resources
	* @param shape Shape to destroy

	*/
	void DestroyGeometry2D(Geometry2D g);

	/*
	* @brief Returns the total points in the geometry
	* @param g Selected shape
	*/
	int Geometry2DTotalPoints(Geometry2D g);

	/*
	* @brief Returns points in the geometry
	* @param g Selected shape
	*/
	float* Geometry2DPoints(Geometry2D g);

	

	/*
	* @brief Draws a shape filled or not.
	* @param shape Shape to render
	* @param filled   Shoud the shape be filled?
	*/
	void DrawSolidShape(Geometry2D shape, bool filled, Transform& transform);
	/*
	* @brief   Draws a line in screen pixels
	* @param x1   Coordinate X for the firts point
	* @param y1   Coordinate Y for the firts point
	* @param x2   Coordinate X for the second point
	* @param y2   Coordinate Y for the second point
	*/
	void DrawLine(int x1, int y1, int x2, int y2);

	/*
	* @brief   Draw a sprite in a X,Y position in screen coordinates
	* @param sprite Desired Sprite
	* @param x  X coordinate
	* @param y  Y coordinate
	*/
	void DrawSprite(Sprite& sprite, int x, int y);

	/*
	* @brief   Draw a sprite in a X,Y position in screen coordinates with an angle
	* @param sprite Desired Sprite
	* @param x  X coordinate
	* @param y  Y coordinate
	* @param angle Angle in radians
	*/
	void DrawSprite(Sprite& sprite, int x, int y, float angle);

	/*
	* @brief   Draw a sprite using a transformation matrix
	* @param sprite Desired Sprite
	* @param transform_mat Transformation matrix
	*/
	void DrawSprite(Sprite& sprite, ALMath::mat4 transform_mat);

	/*
	* @brief   Set the color for draw lines in RGB ( 0 - 255 ) format.
	* @param r   Red value
	* @param g   Green value
	* @param b   Blue value

	*/
	void SetColorStroke(float r, float g, float b);

	/*
	* @brief   Set the color for solid shapes in RGB ( 0 - 255 ) format.
	* @param r   Red value
	* @param g   Green value
	* @param b   Blue value

	*/
	void SetColorFillShape(float r, float g, float b);
	
	/*
	* @brief   Set the color for solid shapes in RGB ( 0 - 255 ) format.
	* @param r   Red value
	* @param g   Green value
	* @param b   Blue value

	*/
	void RenderText(int x, int y, const char* text, float scale, ALMath::vec3 color);

	void RenderText(ALMath::vec2 pos, const char* text, float scale, ALMath::vec3 color);

}

#endif // !_DRAW_

/*
* Copyright 2018. Alejandro Canela.
* Author : Alejandro Canela Mendez - a.canelame@gmail.com
*
* Brief : Basic one file library to manipulate 3D vectors and matrixs.
*/
/*
* Copyright 2018. Alejandro Canela.
* Author : Alejandro Canela Mendez - a.canelame@gmail.com
*
* Brief : Basic one file library to manipulate 3D vectors and matrixs.
*/
#ifndef __MATHL_H__
#define __MATHL_H__

#include <stdio.h>
#include <cstring>
#include <math.h>
#include <assert.h>
namespace ALMath {

#define PI_NUMBER 3.141592653589793238462643383279502884197169399375105820974944592307816406286 
#define DEGREES(x)( (x) * 57.295754f )
#define RADIANS(x)( (x) * 0.174533f )

	union mat4;
	void IdentityMat4(mat4& m);
	void MultMat4(mat4&out, mat4&m1, mat4& m2);

	union vec2 {
		vec2(float x, float y) {
			x_ = x; y_ = y;
		}
		vec2(float v = 0.0f) {
			x_ = y_ = v;
		}
		vec2& operator=(const vec2&v) {
			x_ = v.x_;
			y_ = v.y_;
			return *this;
		}


		vec2 operator*(const vec2& v) {
			return vec2(x_ * v.x_, y_ * v.y_);
		}

		vec2 operator*(float v) {
			return vec2(x_ * v, y_ *v);
		}

		vec2& operator*=(const vec2& v) {
			float f1 = x_ *= v.x_; float f2 = y_ *= v.y_;
			x_ = f1; y_ = f2;
			return *this;
		}
		vec2& operator*=(float v) {
			float f1 = x_ *= v; float f2 = y_ *= v;
			x_ = f1; y_ = f2;
			return *this;
		}
		vec2 operator/(const vec2& v) {
			return vec2(x_ / v.x_, y_ / v.y_);
		}

		vec2 operator/(float v) {
			return vec2(x_ / v, y_ / v);
		}

		vec2& operator/=(const vec2& v) {
			float f1 = x_ /= v.x_; float f2 = y_ /= v.y_;
			x_ = f1; y_ = f2;
			return *this;
		}
		vec2 operator-(const vec2& v) {
			return vec2(x_ - v.x_, y_ - v.y_);
		}
		vec2& operator-=(const vec2& v) {
			float f1 = x_ -= v.x_; float f2 = y_ -= v.y_;
			x_ = f1; y_ = f2;
			return *this;
		}
		vec2 operator+(const vec2& v) {
			return vec2(x_ + v.x_, y_ + v.y_);
		}
		vec2& operator+=(const vec2& v) {
			float f1 = x_ += v.x_; float f2 = y_ += v.y_;
			x_ = f1; y_ = f2;
			return *this;
		}
		float operator[](int index) {
			return m_[index];
		}
		bool operator==(const vec2&v) {
			return (x_ == v.x_ && y_ == v.y_);
		}
		bool operator==(float f) {
			return (x_ == f && y_ == f);
		}
		void log() {
			printf("\n V2LOG: x( %f) y( %f )", x_, y_);
		}

		struct { float x_, y_; };
		float m_[2];
	};


	union vec3 {
		vec3(float x, float y, float z) {
			x_ = x; y_ = y, z_ = z;
		}
		vec3(float  v = 0.0f) {
			x_ = y_ = z_ = v;
		}
		vec3(vec2 v, float v1) {
			x_ = v.x_; y_ = v.y_;  z_ = v1;
		}
		/*vec3(const vec3& v) {
		x_ = v.x_; y_ = v.y_; z_ = v.z_;
		}*/
		vec3& operator=(const vec3&v) {
			x_ = v.x_; y_ = v.y_; z_ = v.z_;
			return *this;
		}
		vec3 operator*(const vec3& v) {
			return vec3(x_ * v.x_, y_ * v.y_, z_ * v.z_);
		}
		vec3& operator*=(const vec3& v) {
			float f1 = x_ *= v.x_; float f2 = y_ *= v.y_; float f3 = z_ *= v.z_;
			x_ = f1; y_ = f2; z_ = f3;
			return *this;
		}
		vec3 operator*(float v) {
			return vec3(x_ * v, y_ *v, z_ *v);
		}
		vec3& operator*=(float v) {
			float f1 = x_ *= v; float f2 = y_ *= v; float f3 = z_ *= v;
			x_ = f1; y_ = f2; z_ = f3;
			return *this;
		}

		vec3 operator/(const vec3& v) {
			return vec3(x_ / v.x_, y_ / v.y_, z_ / v.z_);
		}
		vec3& operator/=(const vec3& v) {
			float f1 = x_ /= v.x_; float f2 = y_ /= v.y_; float f3 = z_ /= v.z_;
			x_ = f1; y_ = f2; z_ = f3;
			return *this;
		}
		vec3 operator-(const vec3& v) {
			return vec3(x_ - v.x_, y_ - v.y_, z_ - v.z_);
		}
		vec3& operator-=(const vec3& v) {
			float f1 = x_ -= v.x_; float f2 = y_ -= v.y_; float f3 = z_ -= v.z_;
			x_ = f1; y_ = f2; z_ = f3;
			return *this;
		}
		vec3 operator+(const vec3& v) {
			return vec3(x_ + v.x_, y_ + v.y_, z_ + v.z_);
		}
		vec3& operator+=(const vec3& v) {
			float f1 = x_ += v.x_; float f2 = y_ += v.y_; float f3 = z_ += v.z_;
			x_ = f1; y_ = f2; z_ = f3;
			return *this;
		}
		float& operator[](int index) {
			return m_[index];
		}
		//float* operator[](int index) {}
		struct { float x_, y_, z_; };
		struct { float r_, g_, b_; };
		float m_[3];
	};

	union vec4 {
		vec4() {
			x_ = 0.0f; y_ = 0.0f; z_ = 0.0f; w_ = 0.0f;
		}
		vec4(float x, float y, float z, float w) {
			x_ = x; y_ = y, z_ = z; w_ = w;
		}
		vec4(float v = 0.0f) {
			x_ = y_ = z_ = w_ = v;
		}
		vec4& operator=(const vec4&v) {
			x_ = v.x_;
			y_ = v.y_;
			z_ = v.z_;
			w_ = v.w_;
			return *this;
		}
		vec4 operator*(const vec4& v) {
			return vec4(x_ * v.x_, y_ * v.y_, z_ * v.z_, w_ *v.w_);
		}
		vec4 operator*(float v) {
			return vec4(x_ * v, y_ *v, z_ *v, w_ * v);
		}
		vec4& operator*=(float v) {
			float f1 = x_ *= v; float f2 = y_ *= v;
			float f3 = z_ *= v; float f4 = w_ *= v;
			x_ = f1; y_ = f2; z_ = f3; w_ = f4;
			return *this;
		}
		vec4& operator*=(const vec4& v) {
			float f1 = x_ *= v.x_; float f2 = y_ *= v.y_;
			float f3 = z_ *= v.z_; float f4 = w_ * v.w_;
			x_ = f1; y_ = f2; z_ = f3; w_ = f4;
			return *this;
		}
		vec4 operator/(const vec4& v) {
			return vec4(x_ / v.x_, y_ / v.y_, z_ / v.z_, w_ / v.w_);
		}
		vec4&  operator/=(const vec4& v) {
			float f1 = x_ /= v.x_; float f2 = y_ /= v.y_;
			float f3 = z_ /= v.z_; float f4 = w_ /= v.w_;
			x_ = f1; y_ = f2; z_ = f3; w_ = f4;
			return *this;;
		}
		vec4 operator-(const vec4& v) {
			return vec4(x_ - v.x_, y_ - v.y_, z_ - v.z_, w_ - v.w_);
		}
		vec4& operator-=(const vec4& v) {
			float f1 = x_ -= v.x_; float f2 = y_ -= v.y_;
			float f3 = z_ -= v.z_; float f4 = w_ -= v.w_;
			x_ = f1; y_ = f2; z_ = f3; w_ = f4;
			return *this;
		}
		vec4 operator+(const vec4& v) {
			return vec4(x_ + v.x_, y_ + v.y_, z_ + v.z_, w_ + v.w_);
		}
		vec4& operator+=(const vec4& v) {
			float f1 = x_ += v.x_; float f2 = y_ += v.y_;
			float f3 = z_ += v.z_; float f4 = w_ += v.w_;
			x_ = f1; y_ = f2; z_ = f3; w_ = f4;
			return *this;
		}
		float& operator[](int index) {
			return m_[index];
		}
		struct { float x_, y_, z_, w_; };
		struct { float r_, g_, b_, a_; };
		struct { float m_[4]; };
	};


	union mat2 {
		mat2() { }
		float& operator[](int index) {
			return m_[index];
		}



		void log() {
			for (int r = 0; r < 4; r += 2) {
				printf("\n %0.2f %0.2f  ", m_[r], m_[r + 1]);
			}
		}
		float m_[4];
		struct { vec2 r[2]; };
	};


	union mat3 {
		mat3() { }
		float& operator[](int index) {
			return m_[index];
		}


		void log() {
			for (int r = 0; r < 9; r += 3) {
				printf("\n %0.2f %0.2f %0.2f ", m_[r], m_[r + 1], m_[r + 2]);
			}
		}

		float m_[9];
		struct { vec3 r[3]; };
	};

	union mat4 {
		mat4() {
			IdentityMat4(*this);
		}
		mat4& operator=(mat4 const &a) {
			r[0] = a.r[0];
			r[1] = a.r[1];
			r[2] = a.r[2];
			r[3] = a.r[3];
			return *this;
		}
		vec4 &operator[](int index) {

			assert(index > 0 || index <3);

			return r[index];
		}




		struct { vec4 r[4]; };


	};



	//functions

	inline void IdentityMat4(mat4&m) {
		m.r[0][0] = 1.0f;
		m.r[0][1] = 0.0f;
		m.r[0][2] = 0.0f;
		m.r[0][3] = 0.0f;

		m.r[1][0] = 0.0f;
		m.r[1][1] = 1.0f;
		m.r[1][2] = 0.0f;
		m.r[1][3] = 0.0f;

		m.r[2][0] = 0.0f;
		m.r[2][1] = 0.0f;
		m.r[2][2] = 1.0f;
		m.r[2][3] = 0.0f;

		m.r[3][0] = 0.0f;
		m.r[3][1] = 0.0f;
		m.r[3][2] = 0.0f;
		m.r[3][3] = 1.0f;
	}

	inline void IdentityMat3(mat3&m) {
		for (int i = 0; i < 9; i++) {
			m[i] = 0;
			if (i % 5 == 0) m[i] = 1;
		}
	}

	inline void IdentityMat2(mat2&m) {
		for (int i = 0; i < 4; i++) {
			m[i] = 0;
			if (i % 3 == 0) m[i] = 1;
		}
	}

	inline void MultMat4(mat4&out, mat4&m1, mat4& m2) {
		mat4 m = {};
		m.r[0] = vec4(
			(m1[0][0] * m2[0][0]) + (m1[1][0] * m2[0][1]) + (m1[2][0] * m2[0][2]) + (m1[3][0] * m2[0][3]),
			(m1[0][1] * m2[0][0]) + (m1[1][1] * m2[0][1]) + (m1[2][1] * m2[0][2]) + (m1[3][1] * m2[0][3]),
			(m1[0][2] * m2[0][0]) + (m1[1][2] * m2[0][1]) + (m1[2][2] * m2[0][2]) + (m1[3][2] * m2[0][3]),
			(m1[0][3] * m2[0][0]) + (m1[1][3] * m2[0][1]) + (m1[2][3] * m2[0][2]) + (m1[3][3] * m2[0][3])
		);

		m.r[1] = vec4(
			(m1[0][0] * m2[1][0]) + (m1[1][0] * m2[1][1]) + (m1[2][0] * m2[1][2]) + (m1[3][0] * m2[1][3]),
			(m1[0][1] * m2[1][0]) + (m1[1][1] * m2[1][1]) + (m1[2][1] * m2[1][2]) + (m1[3][1] * m2[1][3]),
			(m1[0][2] * m2[1][0]) + (m1[1][2] * m2[1][1]) + (m1[2][2] * m2[1][2]) + (m1[3][2] * m2[1][3]),
			(m1[0][3] * m2[1][0]) + (m1[1][3] * m2[1][1]) + (m1[2][3] * m2[1][2]) + (m1[3][3] * m2[1][3])
		);

		m.r[2] = vec4(
			(m1[0][0] * m2[2][0]) + (m1[1][0] * m2[2][1]) + (m1[2][0] * m2[2][2]) + (m1[3][0] * m2[2][3]),
			(m1[0][1] * m2[2][0]) + (m1[1][1] * m2[2][1]) + (m1[2][1] * m2[2][2]) + (m1[3][1] * m2[2][3]),
			(m1[0][2] * m2[2][0]) + (m1[1][2] * m2[2][1]) + (m1[2][2] * m2[2][2]) + (m1[3][2] * m2[2][3]),
			(m1[0][3] * m2[2][0]) + (m1[1][3] * m2[2][1]) + (m1[2][3] * m2[2][2]) + (m1[3][3] * m2[2][3])
		);

		m.r[3] = vec4(
			(m1[0][0] * m2[3][0]) + (m1[1][0] * m2[3][1]) + (m1[2][0] * m2[3][2]) + (m1[3][0] * m2[3][3]),
			(m1[0][1] * m2[3][0]) + (m1[1][1] * m2[3][1]) + (m1[2][1] * m2[3][2]) + (m1[3][1] * m2[3][3]),
			(m1[0][2] * m2[3][0]) + (m1[1][2] * m2[3][1]) + (m1[2][2] * m2[3][2]) + (m1[3][2] * m2[3][3]),
			(m1[0][3] * m2[3][0]) + (m1[1][3] * m2[3][1]) + (m1[2][3] * m2[3][2]) + (m1[3][3] * m2[3][3])
		);

		out = m;
	}

	inline void TranslateMat4(mat4&out, vec3 t) {
		IdentityMat4(out);
		out[3][0] = t.x_;
		out[3][1] = t.y_;
		out[3][2] = t.z_;
	}

	inline void ScaleMat4(mat4&out, vec3 s) {
		IdentityMat4(out);
		out[0][0] = s.x_;
		out[1][1] = s.y_;
		out[2][2] = s.z_;
	}

	inline void TransposeMat4(const mat4&in, mat4& out) {
		mat4 t_in = in;
		mat4 t;
		IdentityMat4(t);
		for (int i = 0; i < 4; i++) {
			for (int a = i + 1; a < 4; a++) {
				int id0 = (i << 2) + a;
				int id1 = (a << 2) + i;
				t[id1] = t_in[id0];
			}
		}
		out = t;


	}

	inline void RotateMat4(mat4&out, vec3 axis = vec3(0.0f), float angle = 0.0f) {
		IdentityMat4(out);
		float c = cos(angle);
		float s = sin(angle);
		if (axis.x_ == 1.0f) {
			out[1][1] = c;
			out[1][2] = -s;
			out[2][1] = s;
			out[2][2] = c;
		}
		else if (axis.y_ == 1.0f) {
			out[0][0] = c;
			out[0][2] = s;
			out[2][0] = -s;
			out[2][2] = c;
		}
		else if (axis.z_ == 1.0f) {
			out[0][0] = c;
			out[0][1] = -s;
			out[1][0] = s;
			out[1][1] = c;
		}
	}
	inline float Dot(const vec2& v1, const vec2& v2) {
		return (v1.x_ * v2.x_) + (v1.y_ * v2.y_);
	}

	inline float Dot(const vec3& v1, const vec3& v2) {
		return (v1.x_ * v2.x_) + (v1.y_ * v2.y_) + (v1.z_ * v2.z_);
	}

	inline float Dot(const vec4& v1, const vec4& v2) {
		return (v1.x_ * v2.x_) + (v1.y_ * v2.y_) + (v1.z_ * v2.z_) + (v1.w_ * v2.w_);
	}



	inline vec3 Cross(const vec3& v1, const vec3& v2) {
		return (v1.x_ * v2.y_) + (v1.y_ * v2.y_) + (v1.z_ * v2.z_);
	}


	inline float Lenght(vec3&v) {
		return sqrt((float)Dot(v, v));
	}

	inline float Lenght(vec4&v) {
		return sqrt((float)Dot(v, v));
	}

	inline float Lenght(vec2&v) {
		return sqrt((float)Dot(v, v));
	}

	inline float LenghtSq(const vec3&v) {
		return Dot(v, v);
	}

	inline float LenghtSq(const vec4&v) {
		return Dot(v, v);
	}

	inline float LenghtSq(const vec2&v) {
		return Dot(v, v);
	}

	inline vec2 Normalize(vec2 &v) {
		float f = Lenght(v);
		return v / f;
	}

	inline vec3 Normalize(vec3& v) {
		float f = Lenght(v);
		return v / f;
	}

	inline vec4 Normalize(vec4& v) {
		float f = Lenght(v);
		return v / f;
	}

	inline vec2 Project(vec2 &lenght, vec2& dir) {
		float d = Dot(lenght, dir);
		float lsq = LenghtSq(dir);
		return dir * (d / lsq);
	}

	inline vec3 Project(vec3 &lenght, vec3& dir) {
		float d = Dot(lenght, dir);
		float lsq = LenghtSq(dir);
		return dir * (d / lsq);
	}

	inline vec2 Perpendicular(vec2 &lenght, vec2& dir) {
		return lenght - Project(lenght, dir);
	}

	inline vec3 Perpendicular(vec3& lenght, vec3& dir) {
		return lenght - Project(lenght, dir);
	}

	inline vec2 Reflection(vec2 &v, vec2& normal) {
		float d = Dot(v, normal);
		vec2 source = normal - v;
		return source - normal * d * 2.0f;
	}

	inline vec3 Reflection(vec3& v, vec3& normal) {
		float d = Dot(v, normal);
		vec3 source = normal - v;
		return source - normal * d * 2.0f;
	}

	inline mat4 LookAt(vec3 origin, vec3 target, vec3 up) {
		mat4 lm;
		IdentityMat4(lm);
		vec3 forward = target - origin;
		vec3 sideway = Cross(forward, up);
		vec3 up_camera = Cross(forward, sideway);

		Normalize(forward);
		Normalize(sideway);
		Normalize(up_camera);

		lm[0] = sideway.x_;
		lm[4] = sideway.y_;
		lm[8] = sideway.z_;

		lm[1] = up_camera.x_;
		lm[5] = up_camera.y_;
		lm[9] = up_camera.z_;

		lm[2] = forward.x_;
		lm[6] = forward.y_;
		lm[10] = forward.z_;

		lm[3] = -target.x_;
		lm[7] = -target.y_;
		lm[11] = -target.z_;
		return lm;
	}
	inline mat4 Frustum(float left, float right, float bottom, float top, float n, float f) {
		mat4 m;
		IdentityMat4(m);
		float m0 = (2.0f * n) / (right - left);
		float m2 = (right + left) / (right - left);
		float m5 = (2.0f * n) / (top - bottom);
		float m6 = (top + bottom) / (top - bottom);
		float m10 = (n + f) / (n - f);
		float m11 = (2.0f * n) / (n - f);
		float m14 = -1.0f;
		m[0] = m0; m[2] = m2;
		m[5] = m5; m[6] = m6;
		m[10] = m10; m[11] = m11;
		m[14] = m14;

	}

	inline mat4 Perspective(float fov, float aspect, float n, float f) {
		mat4 m;
		float a = (fov / 180.0f) * 3.14159f;
		float ff = 0.1f * tan(a * 0.5f);

		m[0] = ff / aspect;
		m[5] = ff;
		m[10] = (f + n) / (n - f);
		m[11] = -1.0f;
		m[14] = (2.0f * f *n) / (n - f);
		return m;
	}

	inline mat4 Ortho(float left, float right, float bottom, float top, float n, float f) {
		mat4 m;

		float m00 = 2.0f / (right - left);
		float m11 = 2.0f / (top - bottom);
		float m22 = -2.0f / (f - n);
		float m30 = -(right + left) / (right - left);
		float m31 = -(top + bottom) / (top - bottom);
		float m32 = -(f + n) / (f - n);

		m[0][0] = m00;
		m[1][1] = m11;
		m[2][2] = m22;

		m[3][0] = m30;
		m[3][1] = m31;
		m[3][2] = m32;
		return m;
	}

	inline mat4 operator*(mat4 const &a, mat4 const &b) {
		mat4 r;
		mat4 r1 = a; mat4 r2 = b;
		MultMat4(r, r1, r2);
		return r;
	}

	inline vec4 operator*(vec4 const &v, mat4 &m) {
		return vec4(v.x_ * m[0][0] + v.y_ * m[1][0] + v.z_ * m[2][0] + v.w_ * m[3][0],
			v.x_ * m[0][1] + v.y_ * m[1][1] + v.z_ * m[2][1] + v.w_ * m[3][1],
			v.x_ * m[0][2] + v.y_ * m[1][2] + v.z_ * m[2][2] + v.w_ * m[3][2],
			v.x_ * m[0][3] + v.y_ * m[1][3] + v.z_ * m[2][3] + v.w_ * m[3][3]);
	}

	inline mat3 operator*(mat3 const &a, mat3 const &b) {

		mat3 t = {};
		for (int r = 0; r < 3; r++) {
			for (int c = 0; c < 3; c++) {
				int idx = (r * 3) + c;
				for (int il = 0; il < 2; il++) {
					int idx1 = (r * 3) + il;
					int idx2 = (il * 3) + c;
					t[idx] += (a.m_[idx1] * b.m_[idx2]);
				}
			}
		}
		return t;
	}

	inline mat2 operator*(mat2 const &a, mat2 const &b) {
		mat2 t;
		for (int r = 0; r < 2; r++) {
			for (int c = 0; c < 2; c++) {
				int idx = (r << 1) + c;
				for (int il = 0; il < 2; il++) {
					int idx1 = (r << 1) + il;
					int idx2 = (il << 1) + c;
					t[idx] += (a.m_[idx1] * b.m_[idx2]);
				}
			}
		}
		return t;
	}



	inline void LogMat4(mat4 m) {
		for (int r = 0; r < 4; r++) {
			printf("\n %f %f %f %f", m[r][0], m[r][1], m[r][2], m[r][3]);
		}
		printf("\n \n");
	}

}

#endif
-- Create Solution

solution "C2D"
configurations{"Debug","Release"}
location("debug")
targetdir("bin")
debugdir("data")


project "C2D"
	kind "ConsoleApp"
	language "C++"
	defines {"_GLFW_WIN32","_GLFW_USE_OPENGL","GLEW_STATIC"}
	libdirs{"lib"}
	links{"opengl32","freetype"}
	includedirs{"include","deps"}
	files{
		"include/**.h",
		"include/**.c",
		"include/**.cc",
		"include/**.inl",

		"deps/**.h",
		"deps/**.c",
		"deps/**.cc",
		"deps/**.inl",
		
		"src/**.cc",
		"src/**.c",
		"src/**.h",
		"src/**.cpp",
	}
	flags{"Symbols"}


project "C2DBuild"
	kind "StaticLib"
	language "C++"
	targetdir("lib")
	defines {"_GLFW_WIN32","_GLFW_USE_OPENGL","GLEW_STATIC"}
	links{"opengl32","freetype"}
	includedirs{"include","deps"}
	files{
		"include/**.h",
		"include/**.c",
		"include/**.cc",
		"include/**.inl",

		"deps/**.h",
		"deps/**.c",
		"deps/**.cc",
		"deps/**.inl",
		
		"src/**.cc",
		"src/**.c",
		"src/**.h",
		"src/**.cpp",
	}

	excludes{"src/main.cc"}
	flags{"Symbols"}



project "C2DTest"
	kind "ConsoleApp"
	language "C++"
	libdirs{"lib"}
	links{"opengl32","C2DBuild","freetype"}
	defines {"_GLFW_WIN32","_GLFW_WGL","_GLFW_USE_OPENGL","GLEW_STATIC","WINDOWS_PLATFORM"}
	includedirs{"include","deps","examples/include"}
	files{
		  "include/**.h",
		  "examples/include/**.h",
		  "examples/src/**.cc",
		  "examples/src/**.c",
		  "examples/src/**.cpp",
		  "examples/src/**.h"
		}
	flags{"Symbols"}

